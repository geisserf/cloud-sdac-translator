
import sys, json, os

def doit(domain, problem, outfile):

    domout = '/'.join(outfile.split('/')[:-1]) + '/domain-out.pddl'
    probout = '/'.join(outfile.split('/')[:-1]) + '/problem-out.pddl'

    try:
        file = open(outfile, 'r')
        output = file.read()
        file.close()
    except Exception, e:
        return json.dumps({'parse_status': 'err',
                                  'error': "Cannot even parse output! %s" % str(e)})

    try:
        file = open(domout, 'r')
        dompddl = file.read()
        file.close()

        file = open(probout, 'r')
        probpddl = file.read()
        file.close()

        return json.dumps({'parse-status': 'ok',
                           'output': output,
                           'domain-out': dompddl,
                           'problem-out': probpddl})

    except Exception, e:
        return json.dumps({'parse_status': 'err', 'output': output,
                           'error': "Failed to read generated pddl-- %s" % str(e)})


if __name__ == '__main__':

    domain = sys.argv[1]
    problem = sys.argv[2]
    solverout = sys.argv[3]

    print doit(domain, problem, solverout)

